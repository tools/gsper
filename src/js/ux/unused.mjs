

/* pop-in handling */
function bgCloseBuilder(side){
    const bgClose = document.createElement("a");
    bgClose.classList.add(`bg${side}`);
    bgClose.classList.add("bgPopupClose");
    bgClose.href = "#";
    bgClose.title = "Fermer";
    return bgClose;
}
document.querySelectorAll('.subPage').forEach( popup => {
    const fermer = document.createElement("a");
    fermer.classList.add("close");
    fermer.href = "#";
    fermer.title = "Fermer";
    fermer.innerHTML = "X";
    popup.appendChild(fermer);
    popup.appendChild(bgCloseBuilder('Top'));
    popup.appendChild(bgCloseBuilder('Left'));
    popup.appendChild(bgCloseBuilder('Right'));
    popup.appendChild(bgCloseBuilder('Bottom'));
} );
document.querySelectorAll('a[href^="#"]').forEach(
    link => link.addEventListener('click',
        e => {
            document.querySelectorAll('.subPage').forEach( section => section.classList.remove("active") );
            e.preventDefault();
            return false;
        }
    )
);
