import focus_1_pubKey, {updatePubKey} from "./1_pubKey.mjs";
import focus_0_home from "./0_home.mjs";
import focus_2_inputSecrets from "./2_inputSecrets.mjs";
import showResult from "./4_result.mjs";
import cleanAll from "./5_cleaning.mjs";
import {stopSearch} from "../logic/search.mjs";

export default function init(){
  // internal navigation
  showPageFromHash();
  window.addEventListener("hashchange",showPageFromHash);
}
const setFocus = [
  focus_0_home,
  focus_1_pubKey,
  focus_2_inputSecrets,
  ()=>0,
  showResult,
  cleanAll,
];

const pages = [
  'page_0_home',
  'page_1_pubKey',
  'page_2_inputSecrets',
  'page_3_inProgress',
  'page_4_result',
  'page_5_cleaning',
];
init();

// internal navigation
function showPageFromHash(){
  try{
    const pubKey = updatePubKey();
    const pageNumber = parseInt(window.location.hash.split('_')[1]);
    if(isNaN(pageNumber)) return window.location = '#page_0_home';
    if(pageNumber<2 || pubKey){
      document.body.className = pages[pageNumber]
    } else return window.location = '#page_0_home';
    setFocus[pageNumber]();
    if(pageNumber!==3) stopSearch();
  } catch (e){
    window.location = '#page_0_home';
    console.error(e);
  }
}
