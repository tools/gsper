import {subscribeToBenchResult} from "../logic/speedBench.mjs";
import prettyMilliseconds from "pretty-ms";
import {timeUnitStrings} from "pretty-ms";
import {sendToWorker, subscribe} from "../logic/workerManager.mjs";
import {getActualData} from "../logic/dico.mjs";
import RangeOption from "./rangeOption.mjs";
import {cacheScale, DEFAULT_CACHE_MAX} from "../logic/search.mjs";
import {saveScale} from "../logic/saveResume.mjs";
import {swapKeyValue} from "../logic/utils.mjs";

const regexDesc = {
  '-1': 'Syntaxe : auto-sélection',
  '0': `Syntaxe : littérale`,
  '1': `Syntaxe : regex`,
  '2': `Syntaxe : combinée`,
}
const mirrorDesc = {
  '-1': 'Clone : auto-sélection',
  '0': `Clone : en miroir uniquement`,
  '1': `Clone : toutes variantes possible`,
}
const accentDesc = {
  '-1': `Accents : auto-sélection`,
  '0': `Accents : inchangés`,
  '1': `Accents : avec et sans`,
  '2': `Accents : fluctuants`,
}
const capsDesc = {
  '-1': `Majuscules : auto-sélection`,
  '0': `Majuscules : inchangées`,
  '1': `Majuscules : classiques`,
  '2': `Majuscules : fluctuantes`,
}


const saveDesc = {
  '-1': `Reprendre : auto-sélection`,
  '0': `Reprendre : désactivé par sécurité`,
  '1': `Reprendre : possible pendant 24h`,
  '2': `Reprendre : possible pendant 48h`,
  '3': `Reprendre : possible pendant 7j`,
}
cacheScale[-1]=DEFAULT_CACHE_MAX;
cacheScale[0]=0;
cacheScale[1]=10_000;
cacheScale[2]=100_000;
cacheScale[3]=1_000_000;
cacheScale[4]=10_000_000;
cacheScale[5]=100_000_000;
cacheScale[6]=1_000_000_000;
const revertCacheScale = swapKeyValue(cacheScale.filter((v,i)=> i>=0 ));

const cacheMaxDesc = {
  '-1': `Cache : auto-sélection`,
  '0': `Cache : désactivé`,
  '1': `Cache : ${cacheScale[1].toLocaleString('fr-FR')}`,
  '2': `Cache : ${cacheScale[2].toLocaleString('fr-FR')}`,
  '3': `Cache : ${cacheScale[3].toLocaleString('fr-FR')}`,
  '4': `Cache : ${cacheScale[4].toLocaleString('fr-FR')}`,
  '5': `Cache : ${cacheScale[5].toLocaleString('fr-FR')}`,
  '6': `Cache : ${cacheScale[6].toLocaleString('fr-FR')}`,
//  '6': `Cache : ${new Intl.NumberFormat().format(cacheScale[6])}`,
}
export function init() {
  const secretsInputNode = document.getElementById('secrets');
  secretsInputNode.addEventListener('keyup', askPreview);
  secretsInputNode.addEventListener('change', askPreview);
  if(window.matchMedia('(min-width: 500px)').matches) document.getElementById('tabDoc').checked = true;
  if(window.matchMedia('(min-width: 720px) and (min-height: 720px)').matches) document.getElementById('tabBrowse').checked = true;
  if(window.matchMedia('(min-width: 1080px) and (min-height: 1080px)').matches) document.getElementById('tabOptions').checked = true;

  window.searchOptions = {
    'accent': new RangeOption('accent', -1, accentDesc, askPreview),
    'caps': new RangeOption('caps', -1, capsDesc, askPreview),
    'mirror': new RangeOption('mirror', -1, mirrorDesc, askPreview),
    'regex': new RangeOption('regex', -1, regexDesc, askPreview),
    'save': new RangeOption('save', -1, saveDesc, askPreview),
    'cacheMax': new RangeOption('cacheMax', -1, cacheMaxDesc, askPreview),
  };
  subscribe('fastPreview',updateFastPreviewDisplay);
  subscribe('smartPreview', updateSmartPreviewDisplay);
  subscribeToBenchResult(askPreview);
  askPreview();
  window.secuTips = {};
  setInterval(secuCheck,200);
}
export default function focus_2_inputSecrets(){
  const toFocus = document.querySelector('#secrets');
  toFocus.focus({preventScroll:true});
}
let lastData = '';
init();

function secuCheck() {
  const tips = Object.keys(window.secuTips).length;
  const elem = document.getElementById('secuTips');
  if(tips){
    elem.innerHTML = `${tips}`;
    elem.classList.remove('hidden');
  } else elem.classList.add('hidden');
}

function askPreview() {
  const actualData = getActualData();
  if(lastData === JSON.stringify(actualData)) return;
  const fastData = JSON.parse(JSON.stringify(actualData));
  delete fastData.options.speed;
  sendToWorker('fastPreview',fastData,0);
  if(!actualData.options.speed) return;
  sendToWorker('smartPreview',actualData,0);
  lastData = JSON.stringify(actualData);
}
function updateFastPreviewDisplay(data){
  const directDico = JSON.parse(data.dico);
  document.getElementById('directAltCount').innerHTML = directDico.length;
  try{window.displayBrowsePreview();}catch (e){}
}
function updateSmartPreviewDisplay(data){
  const directAltCount = parseInt(document.getElementById('directAltCount').innerHTML);
  const smartDico = JSON.parse(data.dico);
  const smartVariantsExtraCount = smartDico.length - directAltCount;

  document.getElementById('smartAltCount').innerHTML = smartVariantsExtraCount ? ` + ${smartVariantsExtraCount} auto suggestions` : '';
  document.getElementById('initialTimeEstimate').innerHTML = `≈ ${timeFormat(smartDico.estimateDuration)}`;

  const sdc = smartDico.config;
  const so = window.searchOptions;
  so.regex.setComputed(sdc.escapeAll===1?0:sdc.escapeAll===0?1:sdc.escapeAll);
  so.mirror.setComputed(sdc.idSecPwd?1:0);
  so.accent.setComputed(sdc.accent || 0);
  so.caps.setComputed(sdc.lowerCase || 0);
  so.accent.setComputed(sdc.accent || 0);
  so.cacheMax.setComputed(revertCacheScale[sdc.cacheMax]);

  try{window.displayBrowsePreview();}catch (e){}
}


timeUnitStrings.By = {short: 'GA', singular: "milliard d'années", plural: "milliards d'années"};
timeUnitStrings.My = {short: 'MA', singular: "million d'années", plural: "millions d'années"};
timeUnitStrings.ky = {short: 'kA', singular: 'millénaire', plural: 'millénaires'};
timeUnitStrings.c = {short: 'sc', singular: 'siècle', plural: 'siècles'};
timeUnitStrings.Y = {short: 'A', singular: 'an', plural: 'ans'};
timeUnitStrings.M = {short: 'M', singular: 'mois', plural: 'mois'};
timeUnitStrings.W = {short: 'sem', singular: 'semaine', plural: 'semaines'};
timeUnitStrings.D = {short: 'j', singular: 'jour', plural: 'jours'};
timeUnitStrings.h = {short: 'h', singular: 'heure', plural: 'heures'};
timeUnitStrings.m = {short: 'm', singular: 'minute', plural: 'minutes'};
timeUnitStrings.s = {short: 's', singular: 'seconde', plural: 'secondes'};
timeUnitStrings.ms = {short: 'ms', singular: 'milliseconde', plural: 'millisecondes'};
timeUnitStrings.µs = {short: 'µs', singular: 'microseconde', plural: 'microsecondes'};
timeUnitStrings.ns = {short: 'ns', singular: 'nanoseconde', plural: 'nanosecondes'};
timeUnitStrings.ps = {short: 'ps', singular: 'picoseconde', plural: 'picosecondes'};

function timeFormat(seconds) {
  return prettyMilliseconds(Math.trunc(seconds * 1000), {
    compact: true,
    verbose: true,
    upToUnit: 'By',
    pluralizeFunc: (n) => n > 1,
  });
}
