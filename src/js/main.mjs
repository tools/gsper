import nav from "./ux/nav.mjs";
import pubKey from "./ux/1_pubKey.mjs";
import inputSecrets from "./ux/2_inputSecrets.mjs";
import inProgress from "./ux/3_inProgress.mjs";
import showResult from "./ux/4_result.mjs";
import _1gallery from "../../node_modules/1gallery/1gallery.mjs";
import speedBench from "./logic/speedBench.mjs";
import giftPopup from "./ux/giftPopup.mjs";
