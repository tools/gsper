// test un lot d'idSec & mdp pour évaluer la vitesse de test des combinaisons.

import {sendToWorker, subscribe, unSubscribe} from "./workerManager.mjs";
import {speedFormat} from "./utils.mjs";


let allExecuted;
let tested = 0;
let combiPerSec = 0;
let departTime;
let lastTime=Date.now();
let lastCount=0;
let globalSpeed;
startBench();

async function startBench(){
  subscribe('batchBruteForce',benchProgress);
  allExecuted=false;
  departTime=Date.now();
  const pub = "AoxVA41dGL2s4ogMNdbCw3FFYjFo5FPK36LuiW1tjGbG";
  let batch = [];
  for(let idSec of pub) for (let pass of pub) {
    if(globalSpeed) return;
    batch.push({idSec,pass});
    if(batch.length===5){
      await sendToWorker('batchBruteForce',{bench:true,batch});
      batch = [];
    }
  }
  if(batch.length) await sendToWorker('batchBruteForce',{bench:true,batch});
  allExecuted=true;
}
function benchProgress(data){
  if(!data.bench) return;
  tested+=data.batch.length;
  const now = Date.now();
  let stop=false;
  if(now-lastTime>1000){
    const kbs = (tested-lastCount)/((now-lastTime)/1000);
    if(kbs>combiPerSec) combiPerSec = kbs;
    else stop=true;
    lastTime=now;
    lastCount=tested;
    console.log(`This computer test ${combiPerSec} key by second.`);
  }
  if(stop || now-departTime>20000 || allExecuted){
    globalSpeed = tested/((now-departTime)/1000);
    console.log(`This computer test ${combiPerSec} key by second at best. ${globalSpeed}kbs average.`);
    finishBench();
  }
}
function finishBench(){
  unSubscribe('batchBruteForce',benchProgress);
  tested = 0;
  document.querySelectorAll('.stepsbar .dot a:nth-child(4)').forEach(dot=>{
    const speedNode = document.createElement("div");
    speedNode.classList.add('estimatedSpeed');
    speedNode.title = "Vitesse estimée";
    speedNode.innerHTML = `${speedFormat(combiPerSec,2)}/s`;
    dot.appendChild(speedNode)
    subscribers.forEach(f=>f(combiPerSec,globalSpeed));
  });
}
const subscribers = [];
export function subscribeToBenchResult(callBack){
  subscribers.push(callBack);
}
export default function getCombiPerSec(){
  if(!globalSpeed) throw new Error("Speed not yet estimated.")
  return combiPerSec;
}
