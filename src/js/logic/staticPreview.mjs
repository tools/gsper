export default function staticPreview(data){
  data.dico.estimateDuration = data.dico.estimateDuration();
  if(!data.previewIndex) data.previewIndex = 0;
  data.dico.preview = {};
  for(let i = -2; i<=2;i++) data.dico.preview = {...data.dico.preview, ...index2Preview(data.previewIndex+i,data.dico)};
}
function index2Preview(i,dico){
  const res = {}, key = (i+dico.length) % dico.length;
  res[`${key}`] = dico.splitDryGet(key);
  return res;
}
