export function swapKeyValue(object) {
  const result = {};
  for (const key in object) {
    result[object[key]] = key;
  }
  return result;
}
export async function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}
export function speedFormat(s,precision=3){
  return s>=Math.pow(10,precision)?s.toFixed(0):s.toPrecision(precision);
}
