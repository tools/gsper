[EN](CHANGELOG.md) | FR

# Changelog | liste des changements
Tous les changements notables de ce projet seront documenté dans ce fichier.

Le format est basé sur [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
et ce projet adhère au [versionnage sémantique](https://semver.org/spec/v2.0.0.html).

## Evolutions probable :
- PWA (site convertible en application mobile utilisable hors ligne)

- sauvegarde automatique (optionnelle) de l'avancement des tests pour reprendre ulterieurement en cas de plantage (activé automatiquement si temps total >1h)
- suppression automatique des sauvegardes après 48h
- suggestion automatique de reprise des tests si sauvegarde présente.

- en cas de doublon détecté via le cache, affichage des doublons contextualisé (combinaison précédentes, suivantes, surlignage dans l'expression régulière) + message d'explication

- export du scénario de test pour calcul distribué (avec pré-répartition en fonction des estimations de vitesse de chaque machine)
- import de scénario de test


Clef publique oubliée ? cherchons là ensemble
- listez vos noms, prénoms, pseudos
- listez le nom de vos certificateurs
- listez le nom de ceux que vous avez certifié
- listez les personnes qui vous ont payé
- listez les personnes que vous avez payé
- où habitez-vous ? (bonus aux localisations proches à partir des localisations extrapolées)
- chercher dans les uid et sur cs+ les info concordantes et proposer par % de match les résultats les plus probables.
  - résultats exacte d'abord, si moins de 10 résultats, ou demande manuelle :
  - résultats des mots peut importe leur ordre, si moins de 10 résultats ou demande manuelle :
  - résultat minuscule désaccentué, si...
  - résultat soundex, si...
  - résultat par première et dernière consonne de chaque mot. si...
  - résultats basé sur les certifs et transaction, directionnelle, si...
  - résultats basé sur les certifs et transaction, peut importe la direction, si...
  - résultat par proximité.
  - Les critères ulterieurs peuvent être utilisé pour pondérer/trier les résultats sans en ajouter de nouveaux.



## [Non-publié/Non-Stabilisé]

### Ajouté

##### Nouveau desgin (par [Ludovic Legros] & [1000i100])
- Page d'accueil
- Page clef publique
- Page saisie des secrets
  - Volet explicatif des fonctions Majuscule Désaccentuer et Avancé
- Page avancement recherche
- Page résultat
- Fil d'ariane navigable
- Design et ergonomie adapté au mobile / interface responsive

##### Interopérabilité (par [1000i100])
- pré-remplissage de la clef publique (pour faire un lien de récupération depuis cesium par exemple) #pubkey=laclef

##### Sécurité (par [1000i100])
- test si le navigateur est connecté à internet lors de la saisi des secrets (et le déconseille)
- test si le navigateur est tor browser (et le recommande pour sa suppression des données locales)

##### Sous le capot (par [1000i100])
- structuration du code pour en faciliter la maintenance et la réutilisation modulaire.
- extraction du code métier (crypto et dico/regex-like) vers G1Lib,
  un packet npm utilisable depuis d'autres projets (Cesium, DunikeyMaker, Gardien...)

## [Version 2.2.1] - 2020-07-02 (par [1000i100])

### Ajouté
- Fichier LICENCE (AGPL-3.0)
- Fichier CHANGELOG.fr.md
- Fichier CHANGELOG.md

## [Version 2.2.0] - 2018-09-14 (par [1000i100])

### Ajouté
- exemple pas à pas du scénario d'utilisation
- si aucune clef publique n'est fournie, un message d'erreur est affiché.

### Modifié
- Retrait de la mention module, superflue après build (meilleur compatibilité)

## [Version 2.1.0] - 2018-06-27 (par [1000i100])

### Ajouté
- syntaxe =référence> pour faire des références syncronisé et éviter de générer des variantes non souhaitées.
- Génération d'une version utilisable hors ligne (meilleur sécurité).

##### DunikeyMaker ajouté en vrac
- afficher clef publique et privé à partir de ses secrets
- créer un fichier d'authentification contenant ses secrets

## [Version 2.0.0] - 2018-05-10 (par [1000i100])

### Ajouté
##### Design / ergonomie
- invitation au don
- lien vers la doc
- titres explicatifs
##### Générateur de variantes de mot de passe
- Déclinaisons avec Majuscules
- Désaccentuation
- Déclinaison avancées façon expression régulière
##### Documentation
- Guide d'utilisation
- Rédaction d'une documentation des générateur de variante de mot de passe
##### Améliorations technique
- Utilisation de Web-Worker (meilleur ergonomie et performance).
- Ajout de test unitaire (meilleur fiabilité).
- Différentiation de la lib pour la partie crypto et de celle de génération de variantes (meilleur maintenabilité et évolutivité).


### Modifié
- Amélioration drastique des performance (grace au webworker notament)
- Export du CSS en feuille de style séparée


## [Version 1.0.1 (Proof of Concept)] - 2018-04-18 (par [1000i100])
### Ajouté

##### Interface unifiée avec
- saisie de la clef publique,
- des identifiants secrets et mots de passe
- nombre de combinaison à tester
- estimatif temporel et d'avancement
- bouton pour cherche la combinaison valide
- lien vers le code source
##### Algorithme
- intégration des librairies de crypto nécessaires
- calcul de la clef publique correspondant à chaque combinaison de secrets saisie, et comparaison à la clef publique de référence.
##### Intégration continue
- publication automatisé de Gsper sous forme de gitlab-pages.

[Non-publié/Non-Stabilisé]: https://framagit.org/1000i100/gsper/-/compare/v2.2.1...master

[Version 2.2.1]: https://framagit.org/1000i100/gsper/-/compare/v2.2.0...v2.2.1
[Version 2.2.0]: https://framagit.org/1000i100/gsper/-/compare/v2.1.0...v2.2.0
[Version 2.1.0]: https://framagit.org/1000i100/gsper/-/compare/v2.0.0...v2.1.0
[Version 2.0.0]: https://framagit.org/1000i100/gsper/-/compare/v1.0.1...v2.0.0
[Version 1.0.1 (Proof of Concept)]: https://framagit.org/1000i100/gsper/-/tree/v1.0.1

[1000i100]: https://framagit.org/1000i100 "@1000i100"
[Ludovic Legros]: https://gitlab.com/ludovic.legrosr "@ludovic.legrosr"
